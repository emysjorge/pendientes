<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page session="false" %>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pendientes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Pendientes" />
    <meta name="keywords" content="Pendientes" />
    <meta name="author" content="Jorge Luis Aguirre Mart�nez" />

    <meta property="og:title" content="Pendientes"/>
    <meta property="og:image" content="https://img.icons8.com/metro/1600/data-pending.png"/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content="Pendientes"/>
    <meta property="og:description" content="Pendientes"/>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="icon" type="image/png" href="https://img.icons8.com/metro/1600/data-pending.png" sizes="32x32">
    <link rel="icon" type="image/png" href="https://img.icons8.com/metro/1600/data-pending.png" sizes="16x16">

    <!-- Google Webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Theme Style -->
	<!-- <link rel="stylesheet" href="resources/css/style.css"> -->
	
	  <style type="text/css">
		/*!
		 * Bootstrap v3.3.5 (http://getbootstrap.com)
		 * Copyright 2011-2015 Twitter, Inc.
		 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
		 */
		/*
		
		/*Mis CSS*/
		
		html {
		  font-family: sans-serif;
		  -ms-text-size-adjust: 100%;
		  -webkit-text-size-adjust: 100%;
		   font-size: 10px;
		  -webkit-tap-highlight-color: transparent;
		}
		
		
		body {
		
		  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		  font-size: 14px;
		  line-height: 1.42857;
		  color: #333333;
		  background-color: #fff;
		  padding-top:20px;
		  margin: 0;
		}
		
		
		.custab{
		    border: 1px solid #ccc;
		    padding: 5px;
		    margin: 5% 0;
		    box-shadow: 3px 3px 2px #ccc;
		    transition: 0.5s;
		    }
		.custab:hover{
		    box-shadow: 3px 3px 0px transparent;
		    transition: 0.5s;
		    }
		    
		    
		a {
		  background-color: transparent;
		}
		
		a {
		  color: #EF5353;
		  text-decoration: none;
		}
		
		a {
		  -webkit-transition: 0.5s;
		  -o-transition: 0.5s;
		  transition: 0.5s;
		}
		
		a:hover, a:focus {
		  color: #e11515;
		  text-decoration: underline;
		}
		a:focus {
		  outline: thin dotted;
		  outline: 5px auto -webkit-focus-ring-color;
		  outline-offset: -2px;
		}
		
		a:active,
		a:hover {
		  outline: 0;
		}
		
		a:hover, a:active, a:focus {
		  outline: none;
		}
		
		
		
		.container {
		  margin-right: auto;
		  margin-left: auto;
		  padding-left: 20px;
		  padding-right: 20px;
		}
		.container:before, .container:after {
		  content: " ";
		  display: table;
		}
		.container:after {
		  clear: both;
		}
		
		
		h1, h2, h3, h4, h5, h6 {
		  color: #000;
		  font-family: "Montserrat", arial, sans-serif;
		}
		
		h1 {
		  font-size: 2em;
		  margin: 0.67em 0;
		}
		
		button,
		input,
		optgroup,
		select,
		textarea {
		  color: inherit;
		  font: inherit;
		  margin: 0;
		}
		
		button {
		  overflow: visible;
		}
		
		button,
		select {
		  text-transform: none;
		}
		
		
		@font-face {
		  font-family: 'Glyphicons Halflings';
		  src: url("../fonts/bootstrap/glyphicons-halflings-regular.eot");
		  src: url("../fonts/bootstrap/glyphicons-halflings-regular.eot?#iefix") format("embedded-opentype"), url("../fonts/bootstrap/glyphicons-halflings-regular.woff2") format("woff2"), url("../fonts/bootstrap/glyphicons-halflings-regular.woff") format("woff"), url("../fonts/bootstrap/glyphicons-halflings-regular.ttf") format("truetype"), url("../fonts/bootstrap/glyphicons-halflings-regular.svg#glyphicons_halflingsregular") format("svg");
		}
		
		
		.form-group {
		  margin-bottom: 30px;
		}
		.form-control {
		  box-shadow: none !important;
		  border: 2px solid #444;
		  background: transparent;
		  font-family: "Montserrat", arial, sans-serif;
		}
		.form-control:hover, .form-control:focus, .form-control:active {
		  outline: none;
		  box-shadow: none !important;
		  border: 2px solid #EF5353;
		}
		
		.form-control {
		  display: block;
		  width: 100%;
		  height: 50px;
		  padding: 14px 12px;
		  font-size: 14px;
		  line-height: 1.42857;
		  color: #555555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		  -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
		  -o-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
		  transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
		}
		.form-control:focus {
		  border-color: #EF5353;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(239, 83, 83, 0.6);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(239, 83, 83, 0.6);
		}
		.form-control::-moz-placeholder {
		  color: #999;
		  opacity: 1;
		}
		.form-control:-ms-input-placeholder {
		  color: #999;
		}
		.form-control::-webkit-input-placeholder {
		  color: #999;
		}
		.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
		  background-color: #eeeeee;
		  opacity: 1;
		}
		.form-control[disabled], fieldset[disabled] .form-control {
		  cursor: not-allowed;
		}
		
		
		.btn-primary {
		  color: #fff;
		  background-color: #EF5353;
		  border-color: #ed3c3c;
		}
		.btn-primary:focus, .btn-primary.focus {
		  color: #fff;
		  background-color: #eb2424;
		  border-color: #9b0e0e;
		}
		.btn-primary:hover {
		  color: #fff;
		  background-color: #eb2424;
		  border-color: #d71414;
		}
		.btn-primary:active, .btn-primary.active, .open > .btn-primary.dropdown-toggle {
		  color: #fff;
		  background-color: #eb2424;
		  border-color: #d71414;
		}
		.btn-primary:active:hover, .btn-primary:active:focus, .btn-primary:active.focus, .btn-primary.active:hover, .btn-primary.active:focus, .btn-primary.active.focus, .open > .btn-primary.dropdown-toggle:hover, .open > .btn-primary.dropdown-toggle:focus, .open > .btn-primary.dropdown-toggle.focus {
		  color: #fff;
		  background-color: #d71414;
		  border-color: #9b0e0e;
		}
		.btn-primary:active, .btn-primary.active, .open > .btn-primary.dropdown-toggle {
		  background-image: none;
		}
		.btn-primary.disabled, .btn-primary.disabled:hover, .btn-primary.disabled:focus, .btn-primary.disabled.focus, .btn-primary.disabled:active, .btn-primary.disabled.active, .btn-primary[disabled], .btn-primary[disabled]:hover, .btn-primary[disabled]:focus, .btn-primary[disabled].focus, .btn-primary[disabled]:active, .btn-primary[disabled].active, fieldset[disabled] .btn-primary, fieldset[disabled] .btn-primary:hover, fieldset[disabled] .btn-primary:focus, fieldset[disabled] .btn-primary.focus, fieldset[disabled] .btn-primary:active, fieldset[disabled] .btn-primary.active {
		  background-color: #EF5353;
		  border-color: #ed3c3c;
		}
		.btn-primary .badge {
		  color: #EF5353;
		  background-color: #fff;
		}
	  </style>
    
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>



<body>
	<div class="container">
	
		<div class="row col-md-6 col-md-offset-2 custyle">

	    <div class="form-group">
			<h3> Agregar Pendiente </h3>     
		</div>
		
  		<c:url var="addAction" value="/pendiente/add" ></c:url>
		
			<form:form action="${addAction}" commandName="pendiente">


				<c:if test="${!empty pendiente.pendiente}">
						<div class="form-group">
							<form:hidden path="idPendiente" />
						</div>
				</c:if>
				
				
				<div class="form-group">
					<form:input path="pendiente" tabindex="1" class="form-control" placeholder="Pendiente" required="required"/>
					<form:errors path="pendiente" cssStyle="color: red;"/>
					<strong style="color: red;">${message}</strong>
				</div>

			     <div class="form-group">
			         <div class="row">
			             <div class="col-sm-6 col-sm-offset-3"  style="text-align:center">
		               			<c:if test="${!empty pendiente.pendiente}">
									<input type="submit" tabindex="2" class="btn btn-primary"
										value="<spring:message text="Editar Pendiente"/>" />
								</c:if>
								<c:if test="${empty pendiente.pendiente}">
									<input type="submit" tabindex="2" class="btn btn-primary"
										value="<spring:message text="Agregar Pendiente"/>" />
								</c:if>
								<!-- <input type="submit" name="addPendiente" id="addPendiente" tabindex="2" class="btn btn-primary" value="Agregar Pendiente"> -->
			             </div>
			         </div>
			     </div>

			</form:form>
		

	
		   	<div class="form-group">
				<h3>Listado de Pendientes</h3> 
			</div>
		
		    <table class="table table-striped custab">
			    <thead>
			        <tr>
			            <th>ID</th>
			            <th>Pendiente</th>
			            <th class="text-center">Acciones</th>
			        </tr>
			    </thead>
	
		   		<c:forEach items="${listPendientes}" var="pendiente">
					<tr>
						<td>${pendiente.idPendiente}</td>
						<td>${pendiente.pendiente}</td>
						<td class="text-center">
							<a href="<c:url value='/edit/${pendiente.idPendiente}' />" class='btn btn-info btn-xs'><span class="glyphicon glyphicon-edit"></span> Editar</a> 
							<a href="<c:url value='/remove/${pendiente.idPendiente}' />" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
						</td>
					</tr>
				</c:forEach>
		    </table>
	    
   			<strong>N�mero de pendientes: ${countList}</strong>
	    			
	    </div>
	</div>
</body>
</html>