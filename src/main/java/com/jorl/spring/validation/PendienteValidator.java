/**
 * 
 */
/**
 * @author EMMX-DELL07
 *
 */
package com.jorl.spring.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.jorl.spring.model.Pendiente;

public class PendienteValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Pendiente.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pendiente", "pendiente.pendiente", "El pendiente no puede estar vacio.");
        
    }
}
