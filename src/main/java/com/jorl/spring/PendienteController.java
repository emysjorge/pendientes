/**
 * 
 */
/**
 * @author EMMX-DELL07
 *
 */
package com.jorl.spring;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jorl.spring.dao.PendienteDAOImpl;
import com.jorl.spring.model.Pendiente;
import com.jorl.spring.service.PendienteService;
import com.jorl.spring.validation.PendienteValidator;

@Controller
public class PendienteController {
	
	private static final Logger logger = LoggerFactory.getLogger(PendienteController.class);
	
	private PendienteService pendienteService;
	private boolean empty = true;

	@Autowired(required=true)
	@Qualifier(value="pendienteService")
	public void setPendienteService(PendienteService pendienteService) {
		this.pendienteService = pendienteService;
	}
	
    @InitBinder("/pendiente/add")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new PendienteValidator());
    }
		
//	@RequestMapping(value = "/pendientes", method = RequestMethod.GET)
//	@RequestMapping(value = {"/pendientes","/"}, method = RequestMethod.GET)
	@RequestMapping (value = {"", "/pendientes"}, method = RequestMethod.GET)
	public String listPersons(Model model) {
		model.addAttribute("pendiente", new Pendiente());
		model.addAttribute("listPendientes", this.pendienteService.listPendientes());
		model.addAttribute("countList", this.pendienteService.countList());
		if(!empty) {
			model.addAttribute("message", "El pendiente no puede estar vac�o.");
		}
		return "pendiente";
	}
	
	//Para agregar y actualizar el pendiente
	@RequestMapping(value= "/pendiente/add", method = RequestMethod.POST)
	public String addPendiente(@ModelAttribute("pendiente") Pendiente p, @Validated Pendiente pendiente, BindingResult result){
		
		empty = true;
		
		if(p.getIdPendiente() == 0){
			
		   //V�lido si el campo realmente no est� vac�o - El campo no puede ir vac�o

		   //si Spring o nosotros hemos detectado error, volvemos al formulario    
		   if (result.hasErrors()) {
		       return "redirect:/pendientes";
		   }
		   
		   if(p.getPendiente() == null || p.getPendiente() == "") {
			   empty = false;
			   logger.info("El pendiente no puede estar vacio.");
			   return "redirect:/pendientes";
		   }
			   
			//Agregar nuevo pendiente
			this.pendienteService.addPendiente(p);;
		}else{
			
		   if(p.getPendiente() == null || p.getPendiente() == "") {
			   empty = false;
			   logger.info("El pendiente no puede estar vacio.");
			   return "redirect:/pendientes";
		   }
			   
			//Actualizar pendiente existente
			this.pendienteService.updatePendiente(p);
		}
		
		return "redirect:/pendientes";	
	}
	
	@RequestMapping("/remove/{id}")
    public String removePendiente(@PathVariable("id") int id){
		
        this.pendienteService.removePendiente(id);
        return "redirect:/pendientes";
    }
 
    @RequestMapping("/edit/{id}")
    public String editPendiente(@PathVariable("id") int id, Model model){
        model.addAttribute("pendiente", this.pendienteService.getPendienteById(id));
        model.addAttribute("listPendientes", this.pendienteService.listPendientes());
        model.addAttribute("countList", this.pendienteService.countList());
        return "pendiente";
    }
}