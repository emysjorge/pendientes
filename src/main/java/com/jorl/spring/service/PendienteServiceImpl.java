package com.jorl.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jorl.spring.dao.PendienteDAO;
import com.jorl.spring.model.Pendiente;

@Service
public class PendienteServiceImpl implements PendienteService {

	private PendienteDAO pendienteDAO;

	public void setPendienteDAO(PendienteDAO pendienteDAO) {
		this.pendienteDAO = pendienteDAO;
	}

	@Override
	@Transactional
	public void addPendiente(Pendiente p) {
		// TODO Auto-generated method stub
		this.pendienteDAO.addPendiente(p);
	}

	@Override
	@Transactional
	public void updatePendiente(Pendiente p) {
		// TODO Auto-generated method stub
		this.pendienteDAO.updatePendiente(p);
	}

	@Override
	@Transactional
	public List<Pendiente> listPendientes() {
		// TODO Auto-generated method stub
		return this.pendienteDAO.listPendientes();
	}

	@Override
	@Transactional
	public int countList() {
		// TODO Auto-generated method stub
		return this.pendienteDAO.countList();
	}
	
	@Override
	@Transactional
	public Pendiente getPendienteById(int id) {
		// TODO Auto-generated method stub
		return this.pendienteDAO.getPendienteById(id);
	}

	@Override
	@Transactional
	public void removePendiente(int id) {
		// TODO Auto-generated method stub
		this.pendienteDAO.removePendiente(id);
	}

}