package com.jorl.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jorl.spring.model.Pendiente;

@Repository
public class PendienteDAOImpl implements PendienteDAO {

	
	private static final Logger logger = LoggerFactory.getLogger(PendienteDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addPendiente(Pendiente p) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(p);
		logger.info("Pendiente guardado exitosamente, Detalles del pendiente = " + p);
	}

	@Override
	public void updatePendiente(Pendiente p) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);
		logger.info("Pendiente actualizado exitosamente, Detalles del pendiente = " + p);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pendiente> listPendientes() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<Pendiente> pendientesList = session.createQuery("from pendiente").list();
		for(Pendiente p : pendientesList){
			logger.info("Pendiente List:: " + p);
		}
		return pendientesList;
	}
	
	@Override
	public int countList() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();		
		int count = session.createQuery("from pendiente").list().size();
		logger.info("Numeros de pendientes - " + count);
		return count;
	}

	@Override
	public Pendiente getPendienteById(int id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();		
		Pendiente p = (Pendiente) session.load(Pendiente.class, new Integer(id));
		logger.info("Pendiente cargado exitosamente, Detalles del pendiente - " + p);
		return p;
	}

	@Override
	public void removePendiente(int id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Pendiente p = (Pendiente) session.load(Pendiente.class, new Integer(id));
		if(null != p){
			session.delete(p);
		}
		logger.info("Pendiente eliminado exitosamente, Detalles del pendiente = " + p);
	}

}