package com.jorl.spring.dao;

import java.util.List;

import com.jorl.spring.model.Pendiente;

public interface PendienteDAO {
	
	public void addPendiente(Pendiente p);
	public void updatePendiente(Pendiente p);
	public List<Pendiente> listPendientes();
	public int countList();
	public Pendiente getPendienteById(int id);
	public void removePendiente(int id);
}