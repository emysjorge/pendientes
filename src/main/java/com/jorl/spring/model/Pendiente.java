package com.jorl.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author Jorge Aguirre
 *
 */
@Entity(name="pendiente")
@Table(name="pendiente")
public class Pendiente {

	@Id
	@Column(name="idPendiente")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idPendiente;
	
	private String pendiente;
	
	
	public int getIdPendiente() {
		return idPendiente;
	}

	public void setIdPendiente(int idPendiente) {
		this.idPendiente = idPendiente;
	}

	
	public String getPendiente() {
		return pendiente;
	}

	public void setPendiente(String pendiente) {
		this.pendiente = pendiente;
	}


	@Override
	public String toString(){
		return "id= " + idPendiente + ", Pendiente= " + pendiente;
	}
}
